<?php

namespace App;

use Bouncer;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Silber\Bouncer\Database\HasRolesAndAbilities;
// use Silber\Bouncer\BouncerFacade as Bouncer;

class User extends Authenticatable
{
  use Notifiable, HasRolesAndAbilities;

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'name', 'email', 'password',
  ];

  /**
   * The attributes that should be hidden for arrays.
   *
   * @var array
   */
  protected $hidden = [
    'password', 'remember_token',
  ];

  public function posts()
  {
    return $this->hasMany(Post::class);
  }

  public function isAdmin()
  {
    // return $this->role === 'admin';
    // return Bouncer::is($this)->an('admin');
    return $this->isAn('admin');
  }

  public function owns(Model $model, $foreignKey = 'user_id')
  {
    return $this->id === $model->$foreignKey;
  }
}
