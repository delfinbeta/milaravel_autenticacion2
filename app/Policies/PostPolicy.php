<?php

namespace App\Policies;

use App\User;
use App\Post;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
  use HandlesAuthorization;

  // public function before(User $user, $ability)
  // {
  //   // if($user->isAdmin()) {
  //   //   return true;
  //   // }
  // }

  /**
   * Determine whether the user can view the post.
   *
   * @param  \App\User  $user
   * @param  \App\Post  $post
   * @return mixed
   */
  // public function view(User $user, Post $post)
  // {
  //   //
  // }

  /**
   * Determine whether the user can create posts.
   *
   * @param  \App\User  $user
   * @return mixed
   */
  // public function create(User $user)
  // {
  //   // return $user->role === 'author';
  //   // return $user->isAn('author');
  // }

  /**
   * Determine whether the user can update the post.
   *
   * @param  \App\User  $user
   * @param  \App\Post  $post
   * @return mixed
   */
  // public function update(User $user, Post $post)
  // {
  //   // return $user->owns($post);
  //   // return $user->isAn('author') && $user->owns($post);
  //   // return $user->can('update-own', Post::class) && $user->owns($post);
  // }

  /**
   * Determine whether the user can delete the post.
   *
   * @param  \App\User  $user
   * @param  \App\Post  $post
   * @return mixed
   */
  public function delete(User $user, Post $post)
  {
    // return $user->owns($post) && !$post->isPublished();
    if($user->can('delete-published', $post)) {
      return true;
    }

    return !$post->isPublished() && $user->can('delete-draft', $post);
  }

  /**
   * Determine whether the user can restore the post.
   *
   * @param  \App\User  $user
   * @param  \App\Post  $post
   * @return mixed
   */
  // public function restore(User $user, Post $post)
  // {
  //   //
  // }

  /**
   * Determine whether the user can permanently delete the post.
   *
   * @param  \App\User  $user
   * @param  \App\Post  $post
   * @return mixed
   */
  // public function forceDelete(User $user, Post $post)
  // {
  //   //
  // }
}
