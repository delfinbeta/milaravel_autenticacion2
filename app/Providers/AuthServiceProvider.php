<?php

namespace App\Providers;

use App\{User, Post};
use App\Policies\PostPolicy;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
  /**
   * The policy mappings for the application.
   *
   * @var array
   */
  protected $policies = [
    // 'App\Model' => 'App\Policies\ModelPolicy',
    'App\Post' => 'App\Policies\PostPolicy',
  ];

  /**
   * Register any authentication / authorization services.
   *
   * @return void
   */
  public function boot()
  {
    $this->registerPolicies();

    // Gate::before(function(User $user) {
    //   if($user->isAdmin()) {
    //     return true;
    //   }
    // });

    // Gate::define('update-post', function(User $user, Post $post) {
    //   return $user->owns($post);
    // });
    // Gate::define('update-post', 'App\Policies\PostPolicy@update');

    // Gate::define('delete-post', 'App\Policies\PostPolicy@delete');
    // Gate::resource('post', PostPolicy::class);  // view, create, update, delete
  }
}
