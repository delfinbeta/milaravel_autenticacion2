<?php

namespace Tests\Feature;

use App\Post;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class CreatePostTest extends TestCase
{
  use RefreshDatabase;

  /** @test 
   ** Comprobar que un admin puede crear un post. */
  public function admin_create_post()
  {
  	$this->withoutExceptionHandling();

  	$admin = $this->createAdmin();

    $this->actingAs($admin);

    $response = $this->post('admin/posts', [
    	'title' => 'New post'
    ]);

    $response->assertStatus(201)->assertSee('Post created');

    tap(Post::first(), function($post) {
      $this->assertNotNull($post, 'The post was no created');

      $this->assertSame('New post', $post->title);
    });

    // $this->assertDatabaseHas('posts', [
    // 	'title' => 'New post'
    // ]);
  }

  /** @test 
   ** Comprobar que un autor puede crear un post. */
  public function author_create_post()
  {
  	$this->withoutExceptionHandling();

  	// $user = $this->createUser(['role' => 'author']);
    $user = $this->createUser();

    // $user->assign('author');
    $user->allow('create', Post::class);

    $this->actingAs($user);

    $response = $this->post('admin/posts', [
    	'title' => 'New post'
    ]);

    $response->assertStatus(201)->assertSee('Post created');

    $this->assertDatabaseHas('posts', [
    	'title' => 'New post'
    ]);
  }

  /** @test 
   ** Comprobar que un usuario no autorizado no puede crear un post. */
  public function unauthorized_no_create_post()
  {
  	// $user = $this->createUser(['role' => 'subscriber']);
    $user = $this->createUser();

    $this->actingAs($user);

    $response = $this->post('admin/posts', [
    	'title' => 'New post'
    ]);

    $response->assertStatus(403);

    // $this->assertDatabaseMissing('posts', [
    // 	'title' => 'New post'
    // ]);

    $this->assertDatabaseEmpty('posts');
  }
}
