<?php

namespace Tests\Feature;

use App\Post;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ListPostsTest extends TestCase
{
  use RefreshDatabase;

  /** @test 
   ** Comprobar que admins pueden ver todos los post. */
  public function admin_view_post()
  {
    $this->withoutExceptionHandling();
    
    $this->actingAs($admin = $this->createAdmin());

    $post1 = factory(Post::class)->create();
    $post2 = factory(Post::class)->create();

    $response = $this->get('admin/posts');

    $response->assertStatus(200)
        ->assertViewIs('admin.posts.index')
        ->assertViewHas('posts', function ($posts) use ($post1, $post2) {
            return $posts->contains($post1) && $posts->contains($post2);
        });

    $this->assertNotRepeatedQueries();
  }

  /** @test 
   ** Comprobar que usuarios pueden ver los post donde son autores. */
  public function author_view_post()
  {
    $this->withoutExceptionHandling();

    $user = $this->createUser();

  	$post1 = factory(Post::class)->create(['user_id' => $user->id]);
  	$post2 = factory(Post::class)->create();
    $post3 = factory(Post::class)->create(['user_id' => $user->id]);
    $post4 = factory(Post::class)->create();

  	$this->actingAs($user);

  	$response = $this->get('admin/posts');

  	$response->assertStatus(200)
  					 ->assertViewIs('admin.posts.index')
  					 ->assertViewHas('posts', function($posts) use ($post1, $post2, $post3, $post4) {
  					 	 return $posts->contains($post1) && !$posts->contains($post2) 
                   && $posts->contains($post3) && !$posts->contains($post4);
  					 });
  }
}
