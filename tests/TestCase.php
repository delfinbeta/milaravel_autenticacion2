<?php

namespace Tests;

use Bouncer;
use App\User;
use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
  use CreatesApplication, DetectRepeatedQueries;

  public function setUp()
  {
  	parent::setUp();

    $this->seed('BouncerSeeder');

  	$this->enableQueryLog();
  }

  public function tearDown()
  {
  	$this->flushQueryLog();

  	parent::tearDown();
  }

  protected function createUser(array $attributes = [])
  {
  	return factory(User::class)->create($attributes);
  }

  protected function createAdmin()
  {
  	// return factory(User::class)->states('admin')->create();
    // $user = factory(User::class)->create();

    // Bouncer::allow('admin')->everything();  // permitir que el rol admin tenga acceso a todo

    // Bouncer::assign('admin')->to($user);  // asignar rol admin al usuario
    // $user->assign('admin');

    // return $user;

    return tap(factory(User::class)->create(), function($user) {
      $user->assign('admin');
    });
  }

  protected function assertDatabaseEmpty($table, $connection = null)
  {
  	$total = $this->getConnection($connection)->table($table)->count();

  	$this->assertSame(0, $total, sprintf("Failed asserting the table [%s] is empty. %s %s rows found.", $table, $total, str_plural('row', $total)));
  }
}
