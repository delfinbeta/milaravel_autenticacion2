<?php

use App\Post;
use Illuminate\Database\Seeder;
use Silber\Bouncer\BouncerFacade as Bouncer;

class BouncerSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    Bouncer::role()->create([
      'name' => 'admin',
      'title' => 'Administrador'
    ]);

    Bouncer::role()->create([
      'name' => 'author',
      'title' => 'Autor'
    ]);

    Bouncer::ability()->createForModel(Post::class, [
      'name' => 'create',
      'title' => 'Crear Posts'
    ]);

    Bouncer::ability()->createForModel(Post::class, [
      'name' => 'update',
      'title' => 'Actualizar Posts'
    ]);

    Bouncer::ability()->createForModel(Post::class, [
      'name' => 'update',
      'title' => 'Actualizar Posts Propios',
      'only_owned' => true
    ]);

    Bouncer::allow('admin')->everything();  // permitir que el rol admin tenga acceso a todo

    Bouncer::allow('author')->to('create', Post::class);

    Bouncer::allow('author')->toOwn(Post::class)->to(['update', 'delete-draft']);

    Bouncer::allow('editor')->to(['update', 'delete-draft'], Post::class);
  }
}
