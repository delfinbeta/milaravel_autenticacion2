<?php

use App\{User, Post};
use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    factory(Post::class)->times(30)->create();

    factory(Post::class)->times(10)->create([
    	'user_id' => User::where('email', 'autor@gmail.com')->first()->id
  	]);
  }
}
