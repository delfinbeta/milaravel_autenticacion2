<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
  /**
   * Run the database seeds.
   *
   * @return void
   */
  public function run()
  {
    $this->createAdmin();
    
    $this->createAuthor();
  }

  protected function createAdmin()
  {
  	$admin = factory(\App\User::class)->create([
    	'email' => 'admin@gmail.com',
    	'name' => 'Administrador'
    ]);

    $admin->assign('admin');
  }

  protected function createAuthor()
  {
  	$admin = factory(\App\User::class)->create([
    	'email' => 'autor@gmail.com',
    	'name' => 'Autor'
    ]);

    $admin->assign('author');
  }
}
